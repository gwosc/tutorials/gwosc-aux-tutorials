# GWOSC Auxiliary Channel Documentation

The full list of auxiliary channels included in this release is found in [this file](./O3_bulk_aux_channel_list.csv). This table lists the following attributes for each channel:

* **Channel**: The name of the channel.
* **Sample Rate (Hz)**: The sample rate of the channel.
* **How data was used in LVK analyses**: How this channel was used in published astrophysical analysis by the LVK collaboration. The data products that are produced using these channels may be used by a wide variety of analyses. For example, noise-subtracted data was used by almost all LVK analyses. Conversely, some data products, such as the magnetic coherence measurements, were only used by a few analyses.  
* **Channel description**: A short description of what the channel is physically measuring.

