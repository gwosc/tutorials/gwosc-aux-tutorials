# GWOSC Auxiliary Channel Tutorials

These are tutorials on how to use LIGO auxiliary data in a variety of applications!
It is reccomended that you first complete the tutorials included in the [GW Open Data Workshop](https://www.gw-openscience.org/odw/odw2022/).
This workshop is avaialble as on [online course](https://gw-odw.thinkific.com/courses/workshop-5).

## Installation of required packages

The recommended way to install the required packages to run these tutorials is with the conda package manager. 
All required packages can be installed using the provided [yaml file](./gwosc_aux_env.yml) and the following command:

```
conda env create -n gwosc_aux_env -f gwosc_aux_env.yml 
```
You can then activate your environment by running 
```
conda activate gwosc_aux_env
```

## Basic data access

We recommend first reading this tutorial on accessing the data.

* [Data access tutorial](./data_access.ipynb) 

## Advanced data analysis tutorials

Note that these tutorials are intended to be educational, and do not include all of the details used in a full analysis.
The tutorials are roughly ordered from least challenging to most challenging.

* [Data quality flag tutorial](./dq_flag.ipynb)
* [Line subtraction tutorial](./line_sub.ipynb)
* [Correlated magnetic noise tutorial](./magnetic_corr.ipynb)
* [Glitch subtraction tutorial](./glitch_sub.ipynb)

If you're interested in learning more, we encourage you to check out the following publications:

* [O3 Data Quality Vetoes Technical Note](https://dcc.ligo.org/LIGO-T2100045/public)
* [O3 Line Subtraction Technical Note](https://dcc.ligo.org/LIGO-T2100058/public)
* [O3 Machine-learning Subtraction Publication](https://arxiv.org/abs/1911.09083)
* [O3 Glitch Subtraction Publication](https://arxiv.org/abs/2207.03429)
* [O3 Correlated Magnetic Noise Publication](https://arxiv.org/abs/2101.12130)
