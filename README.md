# GWOSC Auxiliary Channel Tutorials and Documentation

This repository is designed to be  companion to the [GWOSC O3 Auxiliary Channel Release](https://www.gw-openscience.org/O3/auxiliary/).
Included is information about the channels that make up this release and tutorials on how to access and analyze the provided data. 

* [Channels](./Channels)
* [Tutorials](./Tutorials)

